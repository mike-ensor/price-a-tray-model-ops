# Overview

This project contains the capability to labels images dropped in the GCS bucket. The intent is to have all the source images in a GCS bucket, add classifier labels ("the labels") and annotate each image. The output is then sent to a second bucket or a folder of the bucket. The labels will be exportable to COCO format so they can be used by [MediaPipe Object Detection Customization](https://developers.google.com/mediapipe/solutions/customization/object_detector#coco_format).

## Label Studio

This project uses [Label Studio](https://pypi.org/project/label-studio/) connected to a Google Cloud Storage bucket.

Label options are added via the "Settings > Labeling Interface" using the "Code" method.

> NOTE: Labels in the model and LabelStudio are not setup properly. Media Studio expects Cap letters in Alpha order, then Lower letters. https://youtu.be/X9554zNNtEY?si=QcORGsHXFBhe-BqG&t=452 ...There are some other clean-up methods

```xml
<View>
  <Image name="burger" value="$image" zoomControl="true" zoom="true"/>

  <RectangleLabels name="label" toName="burger" choice="multiple">
    <Label value="background"/>
    <Label value="baked-apple-pie"/>
    <Label value="big-mac"/>
    <Label value="cheeseburger"/>
    <Label value="coke-large"/>
    <Label value="filet-o-fish"/>
    <Label value="french-fries"/>
    <Label value="hamburger"/>
    <Label value="happy-meal"/>
    <Label value="hashbrown"/>
    <Label value="honey-mustard-packet"/>
    <Label value="spicy-buffalo-packet"/>
    <Label value="ketchup-packet"/>
    <Label value="mcchicken"/>
    <Label value="mcnuggets-four-pack"/>
    <Label value="mcnuggets-snack-pack"/>
    <Label value="mcnuggets-twenty-pack"/>
    <Label value="pancake-breakfast"/>
    <Label value="quarter-pounder"/>
    <Label value="sausage-egg-mcmuffin"/>
    <Label value="snack-wrap"/>
  </RectangleLabels>
</View>
```

## One-time

The bucket needs to exist and be reachable by the `./credentials.json` GSA key. Additionally, the bucket needs to be CORS enabled with the following:

```
gsutil cors set cors-config.json gs://${BUCKET_NAME}
```

